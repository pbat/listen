#!/usr/bin/env python3

import subprocess
import threading
import time
import datetime
import queue
import requests
import wave
import struct
import itertools
import json
import traceback
import configparser

RUNNING = True

record_queue = queue.Queue(maxsize=10)

def run_command(cmd):
    return subprocess.run(cmd, shell=True, check=True, capture_output=True)

def record_thread_main():
    count = 0
    while RUNNING:
        try:
            aac_filename = 'record/record{}.aac'.format(count)
            wav_filename = 'record/record{}.wav'.format(count)
            timestamp = datetime.datetime.now()
            run_command('termux-microphone-record -f {} -l 20'.format(aac_filename))
            print('Recording {}'.format(aac_filename))
            time.sleep(5)
            run_command('termux-microphone-record -q')
            print('Recorded {}'.format(aac_filename))
            item = {
                'timestamp': timestamp,
                'aac_filename': aac_filename,
                'wav_filename': wav_filename,
            }
            record_queue.put(item)
        except:
            print('Exception raised in record thread')
            traceback.print_exc()
        count += 1

def list_local_ips():
    ips = run_command("ip addr | grep 'inet' | cut -d' ' -f6 | cut -d/ -f1").stdout.decode('ascii').splitlines()
    ips = [ip for ip in ips if not ip.startswith('fe80:') and not ip.startswith('127.') and not ip == '::1']
    return ips

def upload_audio(item):
    global config
    url = 'https://api.telegram.org/bot' + config['listen']['bot_token'] + '/sendAudio'
    local_ips = list_local_ips()
    data = {
        'chat_id': config['listen']['recipient'],
        'caption': 'Timestamp: {}\nMax frame: {}\nLocal IPs: {}'.format(item['timestamp'].strftime('%H:%M:%S'), item['max_frame'], ', '.join(local_ips)),
    }
    files = {
        'audio': ('audio.aac',
                  open(item['aac_filename'], 'rb'),
                  'video/mp4'),
    }
    r = requests.post(url, data=data, files=files)
    r.raise_for_status()

def update_message(item, last_message_id):
    local_ips = list_local_ips()
    text = 'Timestamp: {}\nMax frame: {}\nLocal IPs: {}'.format(item['timestamp'].strftime('%H:%M:%S'), item['max_frame'], ', '.join(local_ips))
    if last_message_id is None:
        url = 'https://api.telegram.org/bot' + config['listen']['bot_token'] + '/sendMessage'
        data = {
            'chat_id': config['listen']['recipient'],
            'disable_notification': True,
            'text': text,
        }
    else:
        url = 'https://api.telegram.org/bot' + config['listen']['bot_token'] + '/editMessageText'
        data = {
            'chat_id': config['listen']['recipient'],
            'message_id': last_message_id,
            'text': text,
        }
    r = requests.post(url, data=data)
    r.raise_for_status()
    ret = json.loads(r.text)
    if last_message_id is None:
        if ret['ok']:
            return ret['result']['message_id']
    else:
        return last_message_id

def load_wave_frames(item):
    wav = wave.open(item['wav_filename'], 'rb')
    assert wav.getframerate() == 8000
    assert wav.getsampwidth() == 2
    assert wav.getnchannels() == 1
    frames = []
    while True:
        packet = [x[0] for x in struct.iter_unpack('<h', wav.readframes(1000))]
        if len(packet) == 0:
            break
        frames.append(packet)
    item['wave_frames'] = list(itertools.chain.from_iterable(frames))

def analyze_record(item):
    max_frame = max([abs(x) for x in item['wave_frames']])
    item['max_frame'] = max_frame
    return max_frame >= int(config['listen']['sound_threshold'])

def process_thread_main():
    last_message_id = None
    while RUNNING or not record_queue.empty():
        try:
            item = record_queue.get()
            print('Transcoding {} to {}'.format(item['aac_filename'], item['wav_filename']))
            run_command('ffmpeg -i {} -vn -acodec pcm_s16le -ac 1 -ar 8000 -f wav -y {}'.format(item['aac_filename'], item['wav_filename']))
            print('Transcoded {} to {}'.format(item['aac_filename'], item['wav_filename']))
            load_wave_frames(item)
            print('Loaded {} frames from {}'.format(len(item['wave_frames']), item['wav_filename']))
            item['interesting'] = analyze_record(item)
            if item['interesting']:
                print('Record {} is interesting, uploading'.format(item['wav_filename']))
                upload_audio(item)
                last_message_id = None
            else:
                print('Record {} is not interesting, ignoring'.format(item['wav_filename']))
                last_message_id = update_message(item, last_message_id)
        except:
            print('Exception raised in process thread')
            traceback.print_exc()
        run_command('rm {} {} || true'.format(item['aac_filename'], item['wav_filename']))

def main():
    global config
    config = configparser.ConfigParser()
    config.read('config.ini')
    run_command('rm -fr record')
    run_command('mkdir record')
    record_thread = threading.Thread(target=record_thread_main)
    record_thread.start()
    process_thread = threading.Thread(target=process_thread_main)
    process_thread.start()
    # try:
    #     while True:
    #         time.sleep(10)
    # except KeyboardInterrupt:
    #     print('Exiting')
    #     RUNNING = False

if __name__ == '__main__':
    main()
